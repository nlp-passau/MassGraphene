import logging
import os

if os.environ.get('LOG_LEVEL') is not None:
    __log_level = getattr(logging, os.environ.get('LOG_LEVEL'), logging.DEBUG)
else:
    __log_level = logging.DEBUG

logging.basicConfig(
    level=__log_level,
    format='%(asctime)s %(levelname)-8s %(name)-12s: %(message).1000s',
    datefmt='%Y-%m-%d %H:%M:%S'
)

logging.getLogger('urllib3').setLevel(logging.WARNING)
logging.getLogger('requests').setLevel(logging.WARNING)

LOG = logging.getLogger
