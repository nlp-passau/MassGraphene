import requests

from .config import LOG
from .models import WikipediaArticle

__all__ = [
    'Wikipedia',
    'WikipediaException'
]

log = LOG('Wikipedia')


class WikipediaException(Exception):
    def __init__(self, message: str):
        super(WikipediaException, self).__init__(message)


class Wikipedia(object):
    __DEFAULT_HEADERS = {
        'Accept': 'application/json',
        'Content-type': 'application/json'
    }

    def __init__(self, url):
        self.__url = url
        self.__get_wiki_url = url + '/article'

        log.info("Wikipedia service initialized at URL: {}".format(self.__url))

    def get_article(self, article_name: str) -> WikipediaArticle:
        log.debug("Sending request to Wikipedia service for article_name: '{}'".format(article_name))
        r = requests.get(self.__get_wiki_url, params={
            'articleName': article_name,
            'sourceType': 'DUMP'
        })
        if not r.ok:
            log.warning(f"Could not get article because of {r.status_code}")
            raise WikipediaException(f"Could not get article because of {r.status_code}")

        wiki_json = r.json()

        return WikipediaArticle(
            article_name=wiki_json['title'],
            page_id=wiki_json['pageId'],
            content=wiki_json['text']
        )
