from time import time

import requests

from .config import LOG
from .models import WikipediaArticle, WikiRDFExtraction

__all__ = [
    'Graphene',
    'GrapheneException'
]

log = LOG('Graphene')


class GrapheneException(Exception):
    def __init__(self, message: str):
        super(GrapheneException, self).__init__(message)


class Graphene(object):
    __DEFAULT_HEADERS = {
        'Accept': 'text/plain',
        'Content-type': 'application/json'
    }

    def __init__(self, url):
        self.__url = url
        self.__get_rdf_url = url + '/relationExtraction/text'

        log.info("Graphene service initialized at URL: {}".format(self.__url))

    def get_rdf(self, article: WikipediaArticle, coreference: bool = True) -> WikiRDFExtraction:
        log.debug("Sending request to Graphene service with text: '{}'...".format(article.content[:20]))
        started = time()
        r = requests.post(self.__get_rdf_url,
                          json={
                              'doCoreference': coreference,
                              'format': 'RDF',
                              'text': article.content
                          })
        if not r.ok:
            log.warning(f"Could not extract RDF because of {r.status_code}")
            raise GrapheneException(f"Could not extract RDF because of {r.status_code}")

        return WikiRDFExtraction(
            article=article,
            coreferenced=coreference,
            rdf=r.text,
            duration=time() - started
        )
