class WikipediaArticle(object):
    def __init__(self, article_name: str, page_id: int, content: str):
        self.__article_name = article_name
        self.__page_id = page_id
        self.__content = content

    @property
    def article_name(self) -> str:
        return self.__article_name

    @property
    def content(self) -> str:
        return self.__content

    @property
    def page_id(self) -> int:
        return self.__page_id

    def __hash__(self) -> int:
        return hash((self.article_name, self.page_id, self.content))


class WikiRDFExtraction(object):
    def __init__(self, article: WikipediaArticle, coreferenced: bool, rdf: str, duration: int):
        self.__article = article
        self.__coreferenced = coreferenced
        self.__rdf = rdf
        self.__duration = duration

    @property
    def article(self) -> WikipediaArticle:
        return self.__article

    @property
    def coreferenced(self) -> bool:
        return self.__coreferenced

    @property
    def rdf(self) -> str:
        return self.__rdf

    @property
    def duration(self) -> int:
        return self.__duration

    def __hash__(self) -> int:
        return hash((self.article, self.coreferenced))
