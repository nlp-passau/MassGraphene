import argparse
import math
import os
from typing import Dict

import jsonpickle
import pandas as pd
import pymongo

from mass_graphene.config import LOG
from mass_graphene.graphene import Graphene, GrapheneException
from mass_graphene.models import WikiRDFExtraction
from mass_graphene.wikipedia import Wikipedia, WikipediaException

log = LOG("MassGraphene")


def __write_log(p_log: Dict[str, Dict], processing_log: str):
    with open(processing_log, 'w') as o:
        o.write(jsonpickle.encode(p_log))


def __create_id(extraction: WikiRDFExtraction) -> str:
    return f"{extraction.article.page_id}_{hash(extraction)}"


def __store_in_db(db, extraction: WikiRDFExtraction):
    log.info(f"Inserting '{extraction.article.article_name}' to database")
    db.insert_one({
        "_id": __create_id(extraction),
        "title": extraction.article.article_name,
        "original_content": extraction.article.content,
        "rdf": extraction.rdf,
        "is_coreference": extraction.coreferenced,
        "duration": extraction.duration
    })


def __run(input_file: str, output: str, mongodb: str, graphene_url: str, wiki_url: str, max_runs: int = 10,
          __current_run: int = 1):
    db_client = pymongo.MongoClient(host=mongodb)
    db = db_client['WikiRDFExtractions'].extractions

    w = Wikipedia(url=wiki_url)
    g = Graphene(url=graphene_url)

    if os.path.isdir(output):
        log.info(f"Using '{output}' as output folder")
    else:
        os.mkdir(output, mode=0o755)
        log.info(f"Created '{output}' as output folder")

    processing_log = os.path.join(output, 'progress.json')

    if os.path.isfile(processing_log):
        with open(processing_log, 'r') as i:
            p_log = jsonpickle.decode(i.read())
    else:
        p_log = {}

    article_names = []
    with open(input_file, 'r') as i:
        for line in i:
            article_names.append(line.strip())

    if len(article_names) == 0:
        raise ValueError("Input list cannot be read or is empty")

    longest_name = max(len(a) for a in article_names)
    number_format = math.ceil(math.log10(len(article_names)))

    for i, article_name in enumerate(article_names, start=1):
        log.info("Running for article '{{}}'{{}} ({{:0{}d}}/{{:0{}d}})"
                 .format(number_format, number_format)
                 .format(article_name, " " * (longest_name - len(article_name)), i, len(article_names)))

        if article_name not in p_log:
            p_log[article_name] = {}

        if article_name in p_log and 'db_success' in p_log[article_name] and p_log[article_name]['db_success']:
            log.info(f"Skipping article '{article_name}', already in database")
            continue

        try:
            wiki_article = w.get_article(article_name)
            p_log[article_name].update({
                'wiki_success': True
            })
            __write_log(p_log, processing_log)
        except WikipediaException as e:
            p_log[article_name].update({
                'has_error': True,
                'error_message': str(e)
            })
            __write_log(p_log, processing_log)
            continue

        p_log[article_name].update({
            'processed': False
        })
        __write_log(p_log, processing_log)

        try:
            extraction = g.get_rdf(wiki_article, coreference=True)
            p_log[article_name].update({
                'rdf_success': True
            })
            if 'has_error' in p_log[article_name]:
                del p_log[article_name]['has_error']
            if 'error_message' in p_log[article_name]:
                del p_log[article_name]['error_message']
            __write_log(p_log, processing_log)
        except GrapheneException as e:
            p_log[article_name].update({
                'has_error': True,
                'error_message': str(e)
            })
            __write_log(p_log, processing_log)
            continue
        finally:
            p_log[article_name].update({
                'processed': True
            })
            __write_log(p_log, processing_log)

        with open(os.path.join(output, f'{article_name}.n3'), 'w') as o:
            o.write(extraction.rdf)
            try:
                __store_in_db(db, extraction)
                p_log[article_name].update({
                    'db_success': True
                })
            except Exception as e:
                p_log[article_name].update({
                    'db_success': False,
                    'has_error': True,
                    'error_message': str(e)
                })
            finally:
                __write_log(p_log, processing_log)

    df = pd.read_json(processing_log, orient='index')
    serious_fail = False
    fail = False

    if 'wiki_success' not in df:
        log.warning(f"None article was successfully downloaded from Wikipedia.")
        serious_fail = True
    elif len(df[df.wiki_success != 1]) > 0:
        log.warning(f"There were {len(df[df.wiki_success != 1])} unsuccessful Wikipedia downloads.")
        fail = True

    if 'rdf_success' not in df:
        log.warning(f"None article was successfully extracted.")
        serious_fail = True
    elif len(df[df.rdf_success != 1]) > 0:
        log.warning(f"There were {len(df[df.rdf_success != 1])} unsuccessful RDF extractions.")
        fail = True

    if 'db_success' not in df:
        log.warning(f"None article was successfully stored in db.")
        serious_fail = True
    elif len(df[df.db_success != 1]) > 0:
        log.warning(f"There were {len(df[df.db_success != 1])} unsuccessful DB storage tries.")
        fail = True

    if serious_fail:
        log.error(f"Continuing probably doesn't make any sense. Quitting.")

    if fail:
        if __current_run >= max_runs:
            log.error(f"Ran for {__current_run} times, but there were still errors.")
        else:
            log.warning("There was at least one failed attempt. Will rerun again. ({{:0{}d}}/{{:0{}d}})"
                        .format(math.ceil(math.log10(max_runs)), math.ceil(math.log10(max_runs)))
                        .format(__current_run, max_runs))
            __run(input_file, output, mongodb, graphene_url, wiki_url, max_runs, __current_run + 1)


if __name__ == '__main__':
    cli = argparse.ArgumentParser()

    cli.add_argument(
        "--input",
        help="Input file, one article per sentence",
        required=True
    )

    cli.add_argument(
        "--output",
        help="Output folder",
        required=True
    )

    cli.add_argument(
        "--mongodb",
        help="URL to mongodb instance",
        required=True
    )

    cli.add_argument(
        "--processing_log",
        help="Logfile about already processed articles",
        default='processed_log.csv'
    )

    cli.add_argument(
        "--graphene_url",
        help="URL where the Graphene service is running",
        required=True
    )

    cli.add_argument(
        "--wiki_url",
        help="URL where the wikipedia service is running",
        required=True
    )

    args = cli.parse_args()

    __run(input_file=args.input,
          output=args.output,
          mongodb=args.mongodb,
          graphene_url=args.graphene_url,
          wiki_url=args.wiki_url)
